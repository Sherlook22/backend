export const checkURL = (url) => {
    if(!url) return true;
    const strUrl = url.toString()
    return strUrl.match(/\.(jpeg|jpg|gif|png)$/) != null;
}