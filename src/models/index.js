import { sequelize } from '../config/database';
import { Category } from './category.model';
import './post.model';

//#####################################################################//
async function crateBaseCategories() {
    await Category.findOrCreate( {where: { name: 'Programing' }} );
    await Category.findOrCreate( {where: { name: 'Productivity' }} );
    await Category.findOrCreate( {where: { name: 'Books' }} );
    await Category.findOrCreate( {where: { name: 'Film' }} );
}
//#####################################################################//

export async function startConnection() {
    await sequelize.sync( {alter:true} ); //{ force:true }
    await crateBaseCategories();
    console.log("You are connected to database");
}