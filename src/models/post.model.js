import { DataTypes } from 'sequelize';
import { sequelize } from '../config/database';
import { Category } from './category.model';

const Post = sequelize.define('Post', {
  title: {
    type: DataTypes.STRING(150),
    allowNull: false
  },
  content: {
    type: DataTypes.STRING(280),
    allowNull: false
  },
  image: {
    type: DataTypes.STRING(200)
  }
}, {
  updatedAt: false
});

Post.belongsTo(Category);

export { Post };