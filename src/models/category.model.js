import { DataTypes } from 'sequelize';
import { sequelize } from '../config/database';

const Category = sequelize.define('Category', {
  name: {
    type: DataTypes.STRING(100),
    allowNull: false
  }
}, {
  timestamps: false
});

export { Category };