import { Sequelize } from 'sequelize';
import { env } from './const'


export const sequelize = new Sequelize({
    database: env.DB.NAME,
    username: env.DB.USER,
    password: env.DB.PASS,
    host: env.DB.HOST,
    dialect: 'mysql',
    port: env.DB.PORT
});


