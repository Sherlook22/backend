import { Router } from 'express';
import PostController from '../controllers/post.controller';

const postRoutes = Router();
const path = '/posts'

postRoutes.route(`${path}`)
    .get(PostController.index)
    .post(PostController.create)

postRoutes.route(`${path}/:id`)
    .get(PostController.index)
    .patch(PostController.update)
    .delete(PostController.destroy)
    
export { postRoutes };