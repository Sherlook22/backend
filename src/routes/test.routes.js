import { Router } from 'express';
import TestController from '../controllers/test.controller';

const testRoutes = Router();
const path = '/test'

testRoutes.route(`${path}`)
    .get(TestController.index);
    
export { testRoutes };