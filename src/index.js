import app from './app';
import { startConnection } from './models/index';


async function main() {
    try{
        await startConnection();
        await app.listen(app.get('port')); 
        console.log('Server on port', app.get('port'));
    }catch(e) {
        console.error(e);
    }
}

main();