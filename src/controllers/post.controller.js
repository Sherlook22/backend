import { Post } from '../models/post.model';  
import { checkURL } from '../helpers/validateImg';

class PostController {
    async index(req, res) {
        try{
            const {id} = req.params;
            if(!id) {
                const posts = await Post.findAll({
                    attributes: { exclude: ['content'] },
                    order: [['createdAt', 'DESC']],
                });
                res.status(200).json({ posts : posts });
            }
            const post = await Post.findOne({ where: {id} });
            res.status(200).json({ post : !post ? "Error, there is no post with that id" : post });
        } catch(e) {
            res.status(500).json({ err: `Problem to access with DB ${e}` });
        }
    }

    async create(req, res) {
        const { title, content, image, CategoryId } = req.body;
        if(!checkURL(image)) res.status(200).json({ mesj : `The URL image is not valid` });
        try {
            await Post.create({ title, content, image, CategoryId });
            res.status(201).json({ mesj : `The POST has created` });
        } catch(e) {
            res.status(500).json({ err: `Problem to access with DB ${e}` });
        }
    }

    async update(req, res) {
        const { id } = req.params;
        const { title, content, image, CategoryId } = req.body;
        if(!checkURL(image)) res.status(200).json({ mesj : `The URL image is not valid` });
        try {
            const post = await Post.findOne({where: { id }});
            if(!post) {
                res.status(200).json({ mesj : `The post was not found` });
            }
            await post.update({ title, content, image, CategoryId })
            res.status(201).json({ mesj : `The POST was update` }); 
        } catch(e) {
            res.status(500).json({ err: `Problem to access with DB ${e}` });
        }
    }

    async destroy(req, res) {
        const { id } = req.params;
        try {
            const post = await Post.destroy({where: { id }});
            if(!post) {
                res.status(200).json({ mesj : `The POST with ID ${id} could not be deleted` });  
            }
            res.status(201).json({ mesj : `The POST was DELETED` });
        } catch(e) {
            res.status(500).json({ err: `Problem to access with DB ${e}` });
        }
    }
}

export default new PostController;
